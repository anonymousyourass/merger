﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("!!!!!!!!!!!!!!!!!!!!!! ");
            string[] path = new string[] { @"c:\1\1.txt", @"c:\1\2.txt", @"c:\1\3.txt", @"c:\1\4.txt", @"c:\1\5.txt", @"c:\1\6.txt" };

            List<string> merged = new List<string>(0);

            int idx = 0;
           
            while (readLine(path[0], idx) != null)
            {
                //Console.WriteLine("readLine(path[0], idx) != null " + idx);
                string line1 = readLine(path[0], idx);

                string line2 = readLine(path[1], idx);
                line2 = _cutTillFirstCommaReplace1To(line2, idx, 2);
                
                string line3 = readLine(path[2], idx);
                line3 = _cutTillFirstCommaReplace1To(line3, idx, 3);
                
                string line4 = readLine(path[3], idx);
                line4 = _cutTillFirstCommaReplace1To(line4, idx, 4);
                
                string line5 = readLine(path[4], idx);
                line5 = _cutTillFirstCommaReplace1To(line5, idx, 5);
                
                string line6 = readLine(path[5], idx);
                line6 = _cutTillFirstCommaReplace1To(line6, idx, 6);

                merged.Add(line1+line2+line3+line4+line5+line6+";");

                idx++;
                Console.WriteLine("idx++ " + idx);
            }

            string[] merge = merged.ToArray();

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"c:\1\merged.csv"))
            {
                foreach (string line in merge)
                {
                    file.WriteLine(line);
                }
            }
            Console.WriteLine("Done merge!");
        }

        private static string _cutTillFirstCommaReplace1To(string original,int lineNum, int toChange)
        {
            
            if (original != null)
            {
                int idxC = original.IndexOf(',');
                if (idxC > 0)
                {
                    original = original.Substring(idxC, original.Length - idxC-1).Replace("time1", "time" + toChange).Replace("value1,", "value" + toChange);
                }
                return original;
            }
            return null;

        }

        private static string readLine(string path, int lineIdx)
        {
           // Console.WriteLine("readLine " + path + "li " + lineIdx);
            string line = string.Empty;
            if (File.Exists(path))
            {

                // Open the stream and read it back.
                using (StreamReader sr = File.OpenText(path))
                {
                    string s = "";
                    int counter = 0;
                    while ((s = sr.ReadLine()) != null)
                    {
                        if (counter == lineIdx)
                        {
                            return s;
                        }
                        //Console.WriteLine(s);
                        counter++;
                    }
                }
            }
            return null;
        }
    }
}
